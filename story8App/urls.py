from django.urls import path, include
from django.contrib import admin
from .views import *

urlpatterns = [
    path('search/', index, name="index"),
]