from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.
class UnitTestStory8 (TestCase):
	def test_apakah_ada_url_search (self):
		response = Client().get('/search/')
		self.assertEqual (response.status_code,200)
	def test_apakah_templatenya_pakai_ajax_html (self) :
		response = Client().get('/search/')
		self.assertTemplateUsed (response, 'ajax.html')
	def test_apakah_ada_tulisan_searchbook (self):
		request = HttpRequest()
		response = index (request)
		html_response = response.content.decode('utf8')
		self.assertIn ('Search Book', html_response)
	def test_apakah_url_wek_tidak_ada (self):
		response = Client().get('/wek/')
		self.assertEqual (response.status_code,404)

class FunctionalTestStory8 (TestCase):
	# functional testing
	def setUp(self):
		super().setUp()
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		self.browser  = webdriver.Chrome(chrome_options=chrome_options)

	def tearDown(self):
		self.browser.quit()
		super().tearDown()

	def test_apakah_input_techonlogy_jika_disubmit_outputnya_juga_technology(self) :
		self.browser.get ('http://127.0.0.1:8000/search')
		
		input_= self.browser.find_element_by_id('searchbook')

		input_.send_keys('Bodies in Technology')
		input_.send_keys(Keys.RETURN)
		time.sleep(4)
		tabel = self.browser.find_element_by_css_selector('#table2')
		tabelku = self.browser.execute_script('return arguments[0].innerText;', tabel)

		self.assertIn('Bodies in Technology', tabelku)