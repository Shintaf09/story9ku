from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
from .views import login
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User

# Create your tests here.
class UnitTestStory9 (TestCase):
    def test_apakah_ada_url_login (self):
        response = Client().get('/')
        self.assertEqual (response.status_code,200)
    def test_apakah_templatenya_pakai_login_html (self) :
        response = Client().get('/')
        self.assertTemplateUsed (response, 'login.html')
    def test_apakah_nama_fungsi_diviewsnya_login(self):
        found = resolve ('/')
        self.assertEqual(found.func, login)
    def test_apakah_ada_tulisan(self):
        request = HttpRequest()
        response = login (request)
        html_response = response.content.decode('utf8')
        self.assertIn ('Hello, Welcome to My Website! Please Login First', html_response)
    def test_apakah_url_wek_tidak_ada (self):
        response = Client().get('/wek/')
        self.assertEqual (response.status_code,404)
    def test_login_logout(self):
        client = Client()

        username = 'shinta'
        password = 'shintacantik09'
        user = User.objects.create_user(username=username, password=password)

        # Login
        response = client.post('/', {
                'username': 'hahaha',
                'password': 'hahaha'
        })
        response = client.post('/', {
                'username': username,
                'password': password
        })

        # Test if login successful
        response = client.get('/status/')
        self.assertEqual(response.status_code, 200)

        # Logout
        response = client.get('/logout/')

class FunctionalTestStory9(TestCase):# functional testing
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()
    
    def test_login(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(1)
        username = self.browser.find_element_by_id('id_username')
        username.send_keys('shincute')
        password = self.browser.find_element_by_id('id_password')
        password.send_keys('shinshin01')
        submitBtn = self.browser.find_element_by_css_selector('input[type=submit]')
        submitBtn.send_keys(Keys.RETURN)
        time.sleep(1)
        self.assertIn('status', self.browser.page_source)