from django.urls import path, include
from django.contrib import admin
from .views import *

urlpatterns = [
    path('', login, name="login"),
    path('logout/', logout, name="logout"),
]  