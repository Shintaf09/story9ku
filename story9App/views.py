from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from .models import Account
from .forms import LoginForm

# Create your views here.


def login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = User.objects.filter(username=username).exists()
        if user:
            is_authenticate = authenticate(
                username=username, password=password)
            if is_authenticate:
                auth_login(request, is_authenticate)
                return redirect('/status/')
            else:
                print('error_password')
        else:
            print('error_user')
    return render(request, 'login.html')


@login_required
def logout(request):
    auth_logout(request)
    return redirect('/')