from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.
class UnitTestStory7 (TestCase):
	# unit testing
	def test_apakah_ada_url_profil (self):
		response = Client().get('/profil/')
		self.assertEqual (response.status_code,200)
	def test_apakah_templatenya_pakai_profil_html (self) :
		response = Client().get('/profil/')
		self.assertTemplateUsed (response, 'profil.html')
	def test_apakah_nama_fungsi_diviewsnya_index (self):
		found = resolve ('/profil/')
		self.assertEqual(found.func, index)
	def test_apakah_ada_tulisan_ShintaFauziah (self):
		request = HttpRequest()
		response = index (request)
		html_response = response.content.decode('utf8')
		self.assertIn ('Shinta Fauziah', html_response)
	def test_apakah_url_wek_tidak_ada (self):
		response = Client().get('/wek/')
		self.assertEqual (response.status_code,404)

class FunctionalTestStory7 (TestCase):
	# functional testing
	def setUp(self):
		super().setUp()
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.browser  = webdriver.Chrome(chrome_options=chrome_options)

	def tearDown(self):
		self.browser.quit()
		super().tearDown()

	def test_css_background(self):
		self.browser.get('http://127.0.0.1:8000/profil')
		time.sleep(5)
		body = self.browser.find_element_by_css_selector('body')
		background_color = body.value_of_css_property('background-color')
		self.assertEqual(background_color, "rgba(37, 34, 30, 1)")
		self.tearDown()

	def test_light_theme(self):
		self.browser.get('http://127.0.0.1:8000/profil')
		time.sleep(5)
		dropdown= self.browser.find_element_by_css_selector('.dropdown-toggle')
		dropdown.send_keys(Keys.RETURN)
		theme = self.browser.find_element_by_css_selector('.light')
		theme.send_keys(Keys.RETURN)

		body = self.browser.find_element_by_css_selector('body')
		background_color = body.value_of_css_property('background-color')
		self.assertEqual(background_color, "rgba(255, 255, 240, 1)")
		self.tearDown()




